# Pemrograman Berorientasi Objek

Java OOP Program Menambahkan Tiket
---
Java OOP (Object-Oriented Programming) adalah paradigma pemrograman yang berfokus pada pengorganisasian kode dalam bentuk objek yang memiliki atribut dan perilaku yang terkait. Dalam Java OOP, program terdiri dari kelas-kelas yang berinteraksi satu sama lain melalui objek-objek yang saling berkomunikasi

Program yang saya buat disini adalah program menambahkan tiket, yakni tiket pesawat dan kereta api, atribut tujuan,harga dan nama pesawat/kereta diinput oleh user. Istilahnya program ini menambahkan daftar tiket ke dalam menggunakan objek kedalam arraylist.

Demonstrasi Program
---
[Link](https://youtu.be/En3BQ8EDb1Q) Youtube

Konsep OOP
---
**1. Encapsulation**
---
Enkapsulasi adalah konsep untuk menggabungkan atribut dan metode dalam sebuah kelas. Dengan menggunakan enkapsulasi, kita dapat membatasi akses langsung ke atribut dan metode dari luar kelas dan hanya memperbolehkan akses melalui metode publik yang ditentukan.

Implementasi terdapat pada class `user`
```java
class User {
    private String username;
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
```

**2. Package**
---
Mekanisme yang digunakan untuk mengelompokkan dan mengorganisir kelas kelas menjadi unit yang terpisah. 

Implementasi terdapat pada class `Ticket` dan turunannya.
```java
Package traveloka;
```

**3. Inheritance**
---
Class Ticket menjadi abstrak class yang mewalikili ticket umum dengan field destination,price juga menjadi landasan dari superclass. TrainTicket dan FlightTicket merupakan turunan yang menjadi child class dari class Ticket sehingga mewarisi filed/atribut dan method yang dimilikinya.

`abstract class Ticket` menjadi supercals
```java
abstract class Ticket {
    protected String destination;
    protected double price;

    public Ticket(String destination, double price) {
        this.destination = destination;
        this.price = price;
    }

    public abstract void printTicketDetails();
}
```

child class nya adalah `FlightTicket`. Subclass ini memiliki constructor khusus dengan keyword `super` untuk mengakses variable,constructor,dan method yang ada di superclass
```java
class FlightTicket extends Ticket {
    private String airline;

    public FlightTicket(String destination, double price, String airline) {
        super(destination, price);
        this.airline = airline;
    }

    @Override
    public void printTicketDetails() {
        System.out.println("FLIGHT TICKET");
        System.out.println("Destination: " + destination);
        System.out.println("Airline: " + airline);
        System.out.println("Price: $" + price);
    }
}
```

**4. Polymorphism**
---
`Ticket.java`
```java
public abstract void printTicketDetails();
```

@Override menjadi salah satu konsep dari Polymorphism
```java
@Override
    public void printTicketDetails() {
        System.out.println("FLIGHT TICKET");
        System.out.println("Destination: " + destination);
        System.out.println("Airline: " + airline);
        System.out.println("Price: $" + price);
    }
```
