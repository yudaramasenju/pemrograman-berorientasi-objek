package traveloka;

// Kelas untuk tiket pesawat
class FlightTicket extends Ticket {
    private String airline;

    public FlightTicket(String destination, double price, String airline) {
        super(destination, price);
        this.airline = airline;
    }

    @Override
    public void printTicketDetails() {
        System.out.println("FLIGHT TICKET");
        System.out.println("Destination: " + destination);
        System.out.println("Airline: " + airline);
        System.out.println("Price: $" + price);
    }
}
