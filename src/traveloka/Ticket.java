package traveloka;

// Kelas abstrak untuk representasi umum tiket
abstract class Ticket {
    protected String destination;
    protected double price;

    public Ticket(String destination, double price) {
        this.destination = destination;
        this.price = price;
    }

    public abstract void printTicketDetails();
}
