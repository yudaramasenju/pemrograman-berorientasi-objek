package traveloka;

import java.util.ArrayList;
import java.util.List;

// Kelas untuk mengelola daftar tiket
class TicketManager {
    private List<Ticket> tickets;

    public TicketManager() {
        tickets = new ArrayList<>();
    }

    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    public void printAllTickets() {
        if (tickets.isEmpty()) {
            System.out.println("No tickets available.");
        } else {
            for (Ticket ticket : tickets) {
                System.out.println();
                ticket.printTicketDetails();
                System.out.println("----------------------------");
            }
        }
    }

}