package traveloka;

// Kelas untuk tiket kereta
class TrainTicket extends Ticket {
    private String trainName;

    public TrainTicket(String destination, double price, String trainName) {
        super(destination, price);
        this.trainName = trainName;
    }

    @Override
    public void printTicketDetails() {
        System.out.println("TRAIN TICKET");
        System.out.println("Destination: " + destination);
        System.out.println("Train Name: " + trainName);
        System.out.println("Price: $" + price);
    }

}
