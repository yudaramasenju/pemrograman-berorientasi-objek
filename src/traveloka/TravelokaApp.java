package traveloka;

import java.util.Scanner;

// Kelas utama untuk menjalankan aplikasi
public class TravelokaApp {
    private static Scanner scanner = new Scanner(System.in);
    private static TicketManager ticketManager = new TicketManager();
    private static UserManager userManager = new UserManager();
    private static User currentUser = null;

    public static void main(String[] args) {
        clearScreen();
        while (true) {
            if (currentUser == null) {
                System.out.println("1. Register");
                System.out.println("2. Login");
                System.out.println("3. Exit");
            } else {
                System.out.println("1. Add Ticket");
                System.out.println("2. Print All Tickets");
                System.out.println("3. Log Out");
                System.out.println("4. Exit");
            }
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // Mengonsumsi karakter newline dari input sebelumnya
            if (currentUser == null) {
                switch (choice) {
                    case 1:
                        registerUser();
                        break;
                    case 2:
                        loginUser();
                        break;
                    case 3:
                        System.out.println("Exiting...");
                        System.exit(0);
                    default:
                        System.out.println("Invalid Choice.");
                }
            } else {
                switch (choice) {
                    case 1:
                        addTicket();
                        break;
                    case 2:
                        System.out.println("All Tickets:");
                        ticketManager.printAllTickets();
                        break;
                    case 3:
                        currentUser = null;
                        System.out.println("Logged Out.");
                        break;
                    case 4:
                        System.out.println("Exiting...");
                        System.exit(0);
                    default:
                        System.out.println("Invalid choice.");
                }
                System.out.println();
            }
        }
    }

    private static void registerUser() {
        System.out.print("Enter Username : ");
        String username = scanner.nextLine();

        System.out.print("Enter password : ");
        String password = scanner.nextLine();

        User user = new User(username, password);
        boolean succes = userManager.addUser(user);
        if (succes) {
            System.out.println("Registration Succes. Please Login");
        } else {
            System.out.println("Username is Already Exist. Please Choose a different username.");
        }
    }

    private static void loginUser() {
        System.out.print("Enter Username : ");
        String username = scanner.nextLine();

        System.out.print("Enter password : ");
        String password = scanner.nextLine();

        boolean validlogin = userManager.isLoginValid(username, password);
        if (validlogin) {
            currentUser = userManager.getUserByUsername(username);
            System.out.println("Login Succesfull. Welcome" + currentUser.getUsername() + "!");
        } else {
            System.out.println("Invalid Username Or Password");
        }
    }

    private static void addTicket() {
        System.out.print("Enter ticket type (1 - Flight, 2 - Train): ");
        int ticketType = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter destination: ");
        String destination = scanner.nextLine();

        System.out.print("Enter price: ");
        double price = scanner.nextDouble();
        scanner.nextLine();

        if (ticketType == 1) {
            System.out.print("Enter airline: ");
            String airline = scanner.nextLine();
            FlightTicket flightTicket = new FlightTicket(destination, price, airline);
            ticketManager.addTicket(flightTicket);
            System.out.println("Flight ticket added successfully.");
        } else if (ticketType == 2) {
            System.out.print("Enter train name: ");
            String trainName = scanner.nextLine();
            TrainTicket trainTicket = new TrainTicket(destination, price, trainName);
            ticketManager.addTicket(trainTicket);
            System.out.println("Train ticket added successfully.");
        } else {
            System.out.println("Invalid ticket type.");
        }
    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

}