package traveloka;

import java.util.ArrayList;
import java.util.List;

// Kelas untuk pengelolaan pengguna
class UserManager {
    private List<User> users;

    public UserManager() {
        users = new ArrayList<>();
    }

    public boolean addUser(User user) {
        if (getUserByUsername(user.getUsername()) == null) {
            users.add(user);
            return true;
        }
        return false;
    }

    public User getUserByUsername(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

    public boolean isLoginValid(String username, String password) {
        User user = getUserByUsername(username);
        return user != null && user.getPassword().equals(password);
    }
}